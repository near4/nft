use near_contract_standards::non_fungible_token::metadata::{
    NFTContractMetadata, NonFungibleTokenMetadataProvider, TokenMetadata, NFT_METADATA_SPEC
};
use near_contract_standards::non_fungible_token::{Token, TokenId};
use near_contract_standards::non_fungible_token::NonFungibleToken;
use near_sdk::borsh::{self, BorshDeserialize, BorshSerialize};
use near_sdk::collections::LazyOption;
use near_sdk::json_types::ValidAccountId;
use near_sdk::{
    env, near_bindgen, AccountId, BorshStorageKey, PanicOnDefault, Promise, PromiseOrValue
};
near_sdk::setup_alloc!();

#[near_bindgen]
#[derive(BorshDeserialize, BorshSerialize, PanicOnDefault)]
pub struct Contract{
    tokens: NonFungibleToken,
    metadata: LazyOption<NFTContractMetadata>
}
const DATA_IMAGE_SVG_NEAR_ICON: &str = "data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 288 288'%3E%3Cg id='l' data-name='l'%3E%3Cpath d='M187.58,79.81l-30.1,44.69a3.2,3.2,0,0,0,4.75,4.2L191.86,103a1.2,1.2,0,0,1,2,.91v80.46a1.2,1.2,0,0,1-2.12.77L102.18,77.93A15.35,15.35,0,0,0,90.47,72.5H87.34A15.34,15.34,0,0,0,72,87.84V201.16A15.34,15.34,0,0,0,87.34,216.5h0a15.35,15.35,0,0,0,13.08-7.31l30.1-44.69a3.2,3.2,0,0,0-4.75-4.2L96.14,186a1.2,1.2,0,0,1-2-.91V104.61a1.2,1.2,0,0,1,2.12-.77l89.55,107.23a15.35,15.35,0,0,0,11.71,5.43h3.13A15.34,15.34,0,0,0,216,201.16V87.84A15.34,15.34,0,0,0,200.66,72.5h0A15.35,15.35,0,0,0,187.58,79.81Z'/%3E%3C/g%3E%3C/svg%3E";

#[derive(BorshSerialize, BorshStorageKey)]
enum StorageKey{
    NonFungibleToken,
    Metadata,
    TokenMetadata,
    Enumeration,
    Approval
}
#[near_bindgen]
impl Contract{
   #[init]
    pub fn new_default_meta(owner_id: ValidAccountId) -> Self{
        Self::new (
            owner_id,
            NFTContractMetadata {
                spec: NFT_METADATA_SPEC.to_string(),
                name: "Metri Non-fungible token".to_string(),
                symbol: "MT NFT".to_string(),
                icon: Some(DATA_IMAGE_SVG_NEAR_ICON.to_string()),
                base_uri: None,
                reference: None,
                reference_hash: None
            }
        )
    }
    #[init]
    pub fn new(owner_id: ValidAccountId, metadata: NFTContractMetadata) -> Self{
        assert!(!env::state_exists(), "Already init");
        metadata.assert_valid();
        Self{
            tokens: NonFungibleToken::new(
                StorageKey::NonFungibleToken,
                owner_id,
                Some(StorageKey::TokenMetadata),
                Some(StorageKey::Enumeration),
                Some(StorageKey::Approval)
            ),
            metadata: LazyOption::new(StorageKey::Metadata, Some(&metadata))
        }
    }
    #[payable]
    pub fn nft_mint(
        &mut self,
        token_id: TokenId,
        receiver_id: ValidAccountId,
        token_metadata: TokenMetadata
    ) -> Token{
        self.tokens.mint(token_id, receiver_id, Some(token_metadata))
    }
}

near_contract_standards::impl_non_fungible_token_core!(Contract, tokens);
near_contract_standards::impl_non_fungible_token_approval!(Contract, tokens);
near_contract_standards::impl_non_fungible_token_enumeration!(Contract, tokens);

impl NonFungibleTokenMetadataProvider for Contract{
    fn nft_metadata(&self) -> NFTContractMetadata{
        self.metadata.get().unwrap()
    }
}

#[cfg(all(test, not(target_arch = "wasm32")))]
mod tests {
    use near_sdk::test_utils::{accounts, VMContextBuilder};
    use near_sdk::testing_env;
    use near_sdk::MockedBlockchain;

    use super::*;

    const MINT_STORAGE_COST:u128 = 5_470_000_000_000_000_000_000; // 0.005 N

    fn get_context(predecessor_account_id: ValidAccountId) -> VMContextBuilder{
        let mut builder = VMContextBuilder::new();
        builder
        .current_account_id(accounts(0))
        .signer_account_id(predecessor_account_id.clone())
        .predecessor_account_id(predecessor_account_id);
        
        builder
    }

    fn sample_token_metadata()-> TokenMetadata{
        TokenMetadata{
            title: Some("Metri Gold".into()),
            description: Some("Metri Gold".into()),
            media: None,
            media_hash: None,
            copies: Some(1u64),
            issued_at: None,
            expires_at: None,
            starts_at: None,
            updated_at: None,
            extra: None,
            reference: None,
            reference_hash: None
        }
    }

    #[test]
    fn test_new(){
        let mut context = get_context(accounts(1));
        testing_env!(context.build());
        let contract = Contract::new_default_meta(accounts(1).into());
        testing_env!(context.is_view(true).build());
        println!("testnew {:?}", contract.nft_token("1".to_string()));
    }

    #[test]
    #[should_panic(expected="The contract is not initialized")]
    fn test_default(){
        let context = get_context(accounts(1));
        testing_env!(context.build());
        let _contract = Contract::default();
    }

    #[test]
    fn test_mint(){
        let mut context = get_context(accounts(0));
        testing_env!(context.build());
        let mut contract = Contract::new_default_meta(accounts(0).into());
        testing_env!(context
        .storage_usage(env::storage_usage())
        .attached_deposit(MINT_STORAGE_COST)
        .predecessor_account_id(accounts(0))
        .build()
        );

        let token_id = "0".to_string();
        let token = contract.nft_mint(token_id, accounts(0), sample_token_metadata());
        println!("a {}, ", token.token_id);
        println!("b {},{} ", token.owner_id, accounts(0).to_string());
        println!("c {:?}, {:?}", token.metadata.unwrap(), sample_token_metadata());
        println!("d {:?},  ", token.approved_account_ids.unwrap());
        println!("aaa {:?},  ", contract.nft_token("0".to_string()));
       

    }

    #[test]
    fn test_transfer(){
        let mut context = get_context(accounts(0));
        testing_env!(context.build());
        let mut contract = Contract::new_default_meta(accounts(0).into());
        testing_env!(context
        .storage_usage(env::storage_usage())
        .attached_deposit(MINT_STORAGE_COST)
        .predecessor_account_id(accounts(0))
        .build()
        );
        let token_id = "1".to_string();
        let token = contract.nft_mint(token_id.clone(), accounts(0), sample_token_metadata());

        testing_env!(context
            .storage_usage(env::storage_usage())
            .attached_deposit(1)
            .predecessor_account_id(accounts(0))
            .build()
            );

        contract.nft_transfer(accounts(1), token_id.clone(), None, None);
        
        testing_env!(context
        .storage_usage(env::storage_usage())
        .account_balance(env::account_balance())
        .is_view(true)
        .attached_deposit(0)
        .build()
        );

        //if let Some(token) = contract.nft_token(token_id.clone()){
            //println!("kk {:?}", contract.nft_token("0".to_string()));
            println!("kk2 {:?}", contract.nft_token("1".to_string()));
            println!("acc {} {} ", accounts(0).to_string(), accounts(1).to_string());
        // }else{
        //     panic!("token nft not correctly");
        // }
        
    }

    #[test]
    fn test_approve(){
        let mut context = get_context(accounts(0));
        testing_env!(context.build());

        let mut contract = Contract::new_default_meta(accounts(0).into());

        testing_env!(context
        .storage_usage(env::storage_usage())
        .attached_deposit(MINT_STORAGE_COST)
        .predecessor_account_id(accounts(0))
        .build()
        );

        let token_id = "0".to_string();
        let token = contract.nft_mint(token_id.clone(), accounts(0), sample_token_metadata());

        // alice approval bob
        testing_env!(context
            .storage_usage(env::storage_usage())
            .attached_deposit(150_000_000_000_000_000_000)// 000 015N
            .predecessor_account_id(accounts(0))
            .build()
        );
        contract.nft_approve(token_id.clone(), accounts(1), None);

        testing_env!(context
            .storage_usage(env::storage_usage())
            .account_balance(env::account_balance())
            .is_view(true)
            .attached_deposit(0)
            .build()
        );

        println!("approve vvvv {} ", contract.nft_is_approved(token_id, accounts(1), Some(1)));

    }

    #[test]
    fn test_revoke(){
        let mut context = get_context(accounts(0));
        testing_env!(context.build());

        let mut contract = Contract::new_default_meta(accounts(0).into());

        testing_env!(context
        .storage_usage(env::storage_usage())
        .attached_deposit(MINT_STORAGE_COST)
        .predecessor_account_id(accounts(0))
        .build()
        );

        let token_id = "0".to_string();
        let token = contract.nft_mint(token_id.clone(), accounts(0), sample_token_metadata());

        // alice approval bob
        testing_env!(context
            .storage_usage(env::storage_usage())
            .attached_deposit(150_000_000_000_000_000_000)// 000 015N
            .predecessor_account_id(accounts(0))
            .build()
        );
        contract.nft_approve(token_id.clone(), accounts(1), None);

        testing_env!(context
        .storage_usage(env::storage_usage())
        .attached_deposit(1)
        .predecessor_account_id(accounts(0))
        .build()
        );

        // alice revoke bob
        //contract.nft_revoke(token_id.clone(), accounts(1));
        //println!("approve revoke {} ", contract.nft_is_approved(token_id.clone(), accounts(1), Some(1)));

        // revoke all
        contract.nft_revoke_all(token_id.clone());
        println!("approve revoke all {} ", contract.nft_is_approved(token_id.clone(), accounts(1), Some(1)));
    }


}